#include <regex>
#include <iterator>
using namespace std;

#include "mqtthandler.hpp"

MQTTHandler::MQTTHandler(PubSubClient mqttclient, char *clientID)
{
  this->mqttClient = mqttclient;
  
  if(strlen(clientID) == 0)
  {
    snprintf(this->clientID, sizeof(this->clientID), "ESP8266Client-%X", random(0xFFFF));
  }
  else
  {
    memcpy(this->clientID, clientID, strlen(clientID)+1);
  }
}

MQTTHandler::MQTTHandler(PubSubClient mqttclient)
{
  MQTTHandler(mqttclient, "");
}

bool MQTTHandler::Connect(IPAddress server, int port)
{
  this->server = server;
  this->port = port;
  
  this->mqttClient.setServer(this->server, port);
  
  return this->Reconnect();
}

bool MQTTHandler::Disconnect()
{
  return true;
}

bool MQTTHandler::Reconnect()
{
  while(!this->mqttClient.connected())
  {
    if(!this->mqttClient.connect(this->clientID))
    {
      char debuginfo[128];
      snprintf(debuginfo, sizeof(debuginfo), "Could not connect to %s:%u as %s", this->server.toString().c_str(), this->port, this->clientID);
      Serial.println(debuginfo);
      Serial.println("Return code " + this->mqttClient.state());
      delay(5000);
      
    }
  }
}

bool MQTTHandler::IsConnected()
{
  return this->mqttClient.connected();
}

bool MQTTHandler::Publish(char *topic, char *message, int qos)
{
  this->mqttClient.publish(topic, message);
  
  return true;
}

bool MQTTHandler::Subscribe(char *topic)
{
  this->mqttClient.subscribe(topic);
  return true;
}

void MQTTHandler::SetCallback(MQTT_CALLBACK_SIGNATURE)
{
  Serial.println("Test");
  this->mqttClient.setCallback(callback);
}

bool MQTTHandler::ParseTopic(char *topic, vector<char *> *topicTree)
{
  topicTree->clear();

  char *subtree = strtok(topic, "/");

  while(subtree != NULL)
  {
    topicTree->push_back(subtree);
    subtree = strtok(NULL, "/");
    
  }
  
  return true;
  
}
