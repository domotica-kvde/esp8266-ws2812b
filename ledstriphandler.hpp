#include <vector>
using namespace std;

#include <FastLED.h>

#define LEDSTRIP_ALL  -1

class LEDStripHandler
{
  private:
    CRGB *ledstrip;
    int ledstrip_length;
    void Update();
    CRGB ParseColour(int colour);
  public:
    LEDStripHandler(CRGB *ledstrip, int length);
    bool GetValue(int led, vector<int> *results);
    bool SetValue(int led, CRGB value);
    bool SetValue(int led, int value);
    void SetBrightness(int brightness);
};
