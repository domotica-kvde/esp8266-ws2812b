#include <vector>

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <FastLED.h>

#include "mqtthandler.hpp"
#include "networkmanager.hpp"
#include "ledstriphandler.hpp"
using namespace std;

#define CONFIGURE_PIN     D1

#define LEDSTRIP_DATA     D7
#define LEDSTRIP_LENGTH   8

#define DEVICE_NAME   "ledstrip-bureau"

#define INDEX_HOSTNAME      0
#define INDEX_PERIPHERAL    1

#define INDEX_LED_CATEGORY  2
#define INDEX_LED_SUBCATEGORY  3

WiFiClient espClient;
PubSubClient client(espClient);
NetworkManager nm(espClient);
MQTTHandler mqtt = MQTTHandler(client, DEVICE_NAME);
CRGB ledstrip[LEDSTRIP_LENGTH];
LEDStripHandler ledstriphandler = LEDStripHandler(ledstrip, LEDSTRIP_LENGTH);

void setupWiFi()
{
  nm.SetConnectedPin(D0);
  nm.SetSSID("Elsen");
  nm.SetPassword("3E82BBFB240B");
  nm.SetConnectedPin(LED_BUILTIN);
  nm.Connect();
}

void setupMQTT()
{
  char ledTopic[64];
  mqtt.Connect(IPAddress(192, 168, 2, 234), 1883);

  snprintf(ledTopic, sizeof(ledTopic), "%s/led/#", DEVICE_NAME);
  //mqtt.SetCallback(callback);
  client.setCallback(callback);
  mqtt.Subscribe(ledTopic);
}

void setupLEDS()
{
  // sanity check delay - allows reprogramming if accidently blowing power w/leds
  delay(2000);
  FastLED.addLeds<WS2812B, LEDSTRIP_DATA, RGB>(ledstrip, LEDSTRIP_LENGTH);
}

void LEDHandler(vector<char *> *topic, char *payload)
{
  char responseTopic[MQTT_TOPIC_LENGTH];
  char responseValue[4];
  
  if(topic->size() >= INDEX_LED_SUBCATEGORY + 1)
  {
    char *category = topic->at(INDEX_LED_CATEGORY);
    char *subCategory = topic->at(INDEX_LED_SUBCATEGORY);

    if(strcmp(category, "set") == 0)
    {
      if(isNumeric(subCategory))
      {
        int numLED = strtol(subCategory, 0, 10);
        ledstriphandler.SetValue(numLED, strtol(payload, 0, 10));
      }
      else if(strcmp(subCategory, "all") == 0)
      {
        ledstriphandler.SetValue(LEDSTRIP_ALL, strtol(payload, 0, 10));
      }
    }
    else if(strcmp(category, "get") == 0)
    {
      vector<int> values(LEDSTRIP_LENGTH);
      long value = 0;

      if(isNumeric(subCategory))
      {
        if(ledstriphandler.GetValue(strtol(subCategory, 0, 10), &values))
        {
          snprintf(responseTopic, sizeof(responseTopic), "%s/led/value/%u", DEVICE_NAME, subCategory);
          snprintf(responseValue, sizeof(responseValue), "%u", values.at(0));
          mqtt.Publish(responseTopic, responseValue);
        }
      }
      else if(strcmp(subCategory, "all") == 0)
      {
        if(ledstriphandler.GetValue(-1, &values))
        {
          for(int i = 0; i < values.size(); i++)
          {
            snprintf(responseTopic, sizeof(responseTopic), "%s/led/value/%u", DEVICE_NAME, i);
            snprintf(responseValue, sizeof(responseValue), "%u", values.at(i));
            mqtt.Publish(responseTopic, responseValue);
          }
        }
      }
    }
    else if(strcmp(category, "brightness") == 0)
    {
      ledstriphandler.SetBrightness(strtol(payload, 0, 10));
    }
    else
    {
      snprintf(responseTopic, sizeof(responseTopic), "%s/status/error", DEVICE_NAME);
      mqtt.Publish(responseTopic, "Unknown property");
    }
  }
  else
  {
    snprintf(responseTopic, sizeof(responseTopic), "%s/status/error", DEVICE_NAME);
    snprintf(responseValue, sizeof(responseValue), "Use the following topic: /%s/led/<category>/<subcategory", DEVICE_NAME);
    mqtt.Publish(responseTopic, responseValue);
  }
}

void callback(char* topic, byte* payload, unsigned int length)
{
  char value[MQTT_PAYLOAD_LENGTH];

  if(length < MQTT_PAYLOAD_LENGTH)
  {
    memcpy(value, payload, sizeof(payload[0])*length);
    value[length] = '\0';
  
    Serial.println("Received:" + String(topic) + "->" + value + "(" + length + ")");
    
    // Put topic in vector
    vector<char *> topicTree;
    mqtt.ParseTopic(topic, &topicTree);
  
    if(strcmp(topicTree.at(1), "status") == 0) // Ignore status topic
    {
    }
    else if(strcmp(topicTree.at(1), "led") == 0)
    {
      LEDHandler(&topicTree, value);
    }
    else
    {
      char responseTopic[64];
      snprintf(responseTopic, sizeof(responseTopic), "%s/status/error", DEVICE_NAME);
      mqtt.Publish(responseTopic, "Unknown topic");
    }
  }
}

bool isNumeric(char *value)
{
  for(int i = 0; i < sizeof(value); i++)
  {
    if(isalpha(value[i]))
    {
      return false;
    }
  }

  return true;
}

void setup() {
  Serial.begin(115200);

  pinMode(LED_BUILTIN, OUTPUT);

  setupWiFi();

  setupMQTT();

  setupLEDS();
}

void loop()
{
  client.loop();
  
  if(!mqtt.IsConnected())
  {
    mqtt.Reconnect();
  }
}
