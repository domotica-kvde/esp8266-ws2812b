#include "networkmanager.hpp"

NetworkManager::NetworkManager(WiFiClient client)
{
}

void NetworkManager::SetSSID(char *ssid)
{
  strncpy(this->ssid, ssid, sizeof(this->ssid));
}

void NetworkManager::SetPassword(char *password)
{
  strncpy(this->password, password, sizeof(this->password));
}

int NetworkManager::Connect()
{
  if(sizeof(this->ssid) == 0)
  {
    Serial.println("Could not connect to network, ssid is empty.");
    return -1;
  }

  if(sizeof(this->password) == 0)
  {
    Serial.println("Could not connect to network, password is empty.");
    return -1;
  }
  WiFi.begin(this->ssid, this->password);

  Serial.print("Connecting to ");
  Serial.println(this->ssid);
  
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }

  if(this->connectedPin > 0)
  {
    digitalWrite(this->connectedPin, HIGH);
  }

  this->IP = WiFi.localIP();
  Serial.print("Connected to ");
  Serial.print(this->ssid);
  Serial.print(" with IP ");
  Serial.println(this->IP.toString());

  return 0;
}

bool NetworkManager::Connect(char *ssid, char *password)
{
  this->SetSSID(ssid);
  this->SetPassword(password);

  return this->Connect();
}

void NetworkManager::SetConnectedPin(int pin)
{
  this->connectedPin = pin;
  pinMode(this->connectedPin, OUTPUT);
  digitalWrite(this->connectedPin, LOW);
}
