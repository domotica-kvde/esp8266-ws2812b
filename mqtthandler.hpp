#include <ESP8266WiFi.h>
#include <PubSubClient.h>
using namespace std;

#define MQTT_TOPIC_LENGTH     64
#define MQTT_PAYLOAD_LENGTH   64
#define MQTT_CLIENTID_LENGTH  64

class MQTTHandler
{
  private:
    PubSubClient mqttClient;
    char clientID[MQTT_CLIENTID_LENGTH];
    IPAddress server;
    int port;
  public:
    MQTTHandler(PubSubClient mqttclient);
    MQTTHandler(PubSubClient mqttclient, char *clientID);
    bool Connect(IPAddress server, int port = 1883);
    bool Disconnect();
    bool Reconnect();
    bool IsConnected();
    bool Publish(char *topic, char *message, int qos = 0);
    bool Subscribe(char *topic);
    void SetCallback(MQTT_CALLBACK_SIGNATURE);
    bool ParseTopic(char *topic, vector<char *> *topicTree);
};
