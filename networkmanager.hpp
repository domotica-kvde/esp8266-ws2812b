#include <ESP8266WiFi.h>

class NetworkManager
{
  private:
    WiFiClient client;
    char ssid[32];
    char password[64];
    IPAddress IP;
    int connectedPin = -1;
  public:
    bool isClient = true;
    
    NetworkManager(WiFiClient client);
    void SetSSID(char *ssid);
    void SetPassword(char *password);
    int Connect();
    bool Connect(char *ssid, char *password);
    void SetConnectedPin(int pin);
};
