#include "ledstriphandler.hpp"

LEDStripHandler::LEDStripHandler(CRGB *ledstrip, int length)
{
  this->ledstrip = ledstrip;
  this->ledstrip_length = length;
}

bool LEDStripHandler::GetValue(int led, vector<int> *results)
{
  if(results == NULL)
  {
    Serial.println("results is NULL");
    return false;
  }

  results->clear();

  // If the required LED is not part of our LED strip, return 0;
  if(led >= this->ledstrip_length)
  {
    return false;
  }
  else if(led == LEDSTRIP_ALL)
  {
    for(int i = 0; i < this->ledstrip_length; i++)
    {
      long value = 0;
      value |= this->ledstrip[i].r << 8;
      value |= ledstrip[i].g << 16;
      value |= ledstrip[i].b;
      results->push_back(value);
    }
  }
  else if(led >= 0) // Individual LED
  {
    results->push_back(0);
    results->at(0) = 0;
    results->at(0) |= this->ledstrip[led].r << 8;
    results->at(0) |= this->ledstrip[led].g << 16;
    results->at(0) |= this->ledstrip[led].b;
    Serial.println(this->ledstrip[led]);
  }
  else
  {
    return false;
  }
  
  for(int i = 0; i < results->size(); i++)
  {
    Serial.println("R: " + String(results->at(i)));
  }

  return true;
}

bool LEDStripHandler::SetValue(int led, int value)
{
  return this->SetValue(led, CRGB(value));
}

bool LEDStripHandler::SetValue(int led, CRGB value)
{
  if(led >= this->ledstrip_length)
  {
    return false;
  }
  else if(led == LEDSTRIP_ALL) // We're using negative numbers for all the LEDs
  {
    fill_solid(this->ledstrip, this->ledstrip_length, value);
  }
  else if(led >= 0) // Individual LED
  {
    this->ledstrip[led] = value;
  }
  else
  {
    return false;
  }

  this->Update();
  
  return true;
}

void LEDStripHandler::SetBrightness(int brightness)
{
  FastLED.setBrightness(brightness);
  this->Update();
}

void LEDStripHandler::Update()
{
  FastLED.show();
}


CRGB LEDStripHandler::ParseColour(int colour)
{
  return CRGB(colour);
}
